﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp20
{
    public partial class Erinupp : Button
    {
        public DataGridView Tabel {
            get => Name == "esitatud" || Name == "vaataOmi" ?
                (Form.ActiveForm as mastervaade1).omaAruanded : 
                (Form.ActiveForm as mastervaade1).teisteAruanded;
        }

        public Erinupp()
        {
            InitializeComponent();
        }
               
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
