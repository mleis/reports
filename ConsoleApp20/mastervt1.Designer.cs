﻿namespace ConsoleApp20
{
    partial class mastervaade1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.otsinguNimi = new System.Windows.Forms.TextBox();
            this.otsiNupp = new System.Windows.Forms.Button();
            this.omaAruanded = new System.Windows.Forms.DataGridView();
            this.teisteAruanded = new System.Windows.Forms.DataGridView();
            this.sisselogiPaneel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.sisselogimisNupp = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.redigeeriPaneel = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.failieemaldamisNupp = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.faililisamisNupp = new System.Windows.Forms.Button();
            this.loobuNupp = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.salvestamiseNupp = new System.Windows.Forms.Button();
            this.muukulu = new System.Windows.Forms.TextBox();
            this.majutuskulu = new System.Windows.Forms.TextBox();
            this.transpordikulu = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uueReisiNupp = new System.Windows.Forms.Button();
            this.muutmisNupp = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.kinnitusPaneel = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.pildivaatamisNupp = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.loobuNupp2 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.väljalogimisNupp = new System.Windows.Forms.Button();
            this.töötajaLisamisNupp = new System.Windows.Forms.Button();
            this.töötajaLisamisPaneel = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.loobuNupp3 = new System.Windows.Forms.Button();
            this.lisamisNupp = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.otsinguAlgus = new System.Windows.Forms.DateTimePicker();
            this.otsinguLõpp = new System.Windows.Forms.DateTimePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.vaataOmi = new ConsoleApp20.Erinupp();
            this.esitatud = new ConsoleApp20.Erinupp();
            this.välja_makstud = new ConsoleApp20.Erinupp();
            this.esitamata = new ConsoleApp20.Erinupp();
            this.kinnitatud = new ConsoleApp20.Erinupp();
            this.vaataTeisteomi = new ConsoleApp20.Erinupp();
            ((System.ComponentModel.ISupportInitialize)(this.omaAruanded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teisteAruanded)).BeginInit();
            this.sisselogiPaneel.SuspendLayout();
            this.redigeeriPaneel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.kinnitusPaneel.SuspendLayout();
            this.töötajaLisamisPaneel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TERE TULEMAST, MASTERUSER";
            // 
            // otsinguNimi
            // 
            this.otsinguNimi.Location = new System.Drawing.Point(7, 58);
            this.otsinguNimi.Name = "otsinguNimi";
            this.otsinguNimi.Size = new System.Drawing.Size(127, 20);
            this.otsinguNimi.TabIndex = 2;
            // 
            // otsiNupp
            // 
            this.otsiNupp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.otsiNupp.Font = new System.Drawing.Font("Perpetua Titling MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.otsiNupp.Location = new System.Drawing.Point(7, 179);
            this.otsiNupp.Name = "otsiNupp";
            this.otsiNupp.Size = new System.Drawing.Size(127, 28);
            this.otsiNupp.TabIndex = 3;
            this.otsiNupp.Text = "OK";
            this.otsiNupp.UseVisualStyleBackColor = false;
            this.otsiNupp.Click += new System.EventHandler(this.otsiNupp_Click);
            // 
            // omaAruanded
            // 
            this.omaAruanded.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.omaAruanded.Location = new System.Drawing.Point(155, 298);
            this.omaAruanded.Name = "omaAruanded";
            this.omaAruanded.Size = new System.Drawing.Size(543, 199);
            this.omaAruanded.TabIndex = 4;
            this.omaAruanded.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.omaAruanded_CellClick);
            this.omaAruanded.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.omaAruanded_CellDoubleClick);
            // 
            // teisteAruanded
            // 
            this.teisteAruanded.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.teisteAruanded.Location = new System.Drawing.Point(155, 56);
            this.teisteAruanded.Name = "teisteAruanded";
            this.teisteAruanded.Size = new System.Drawing.Size(543, 213);
            this.teisteAruanded.TabIndex = 12;
            this.teisteAruanded.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.teisteAruanded_CellClick);
            this.teisteAruanded.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.teisteAruanded_CellDoubleClick);
            // 
            // sisselogiPaneel
            // 
            this.sisselogiPaneel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.sisselogiPaneel.Controls.Add(this.label2);
            this.sisselogiPaneel.Controls.Add(this.sisselogimisNupp);
            this.sisselogiPaneel.Controls.Add(this.textBox2);
            this.sisselogiPaneel.Location = new System.Drawing.Point(0, 0);
            this.sisselogiPaneel.Name = "sisselogiPaneel";
            this.sisselogiPaneel.Size = new System.Drawing.Size(360, 240);
            this.sisselogiPaneel.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "kasutaja:";
            // 
            // sisselogimisNupp
            // 
            this.sisselogimisNupp.Location = new System.Drawing.Point(140, 117);
            this.sisselogimisNupp.Name = "sisselogimisNupp";
            this.sisselogimisNupp.Size = new System.Drawing.Size(75, 29);
            this.sisselogimisNupp.TabIndex = 1;
            this.sisselogimisNupp.Text = "SISENE";
            this.sisselogimisNupp.UseVisualStyleBackColor = true;
            this.sisselogimisNupp.Click += new System.EventHandler(this.sisselogimisNupp_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(117, 91);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(128, 20);
            this.textBox2.TabIndex = 0;
            // 
            // redigeeriPaneel
            // 
            this.redigeeriPaneel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.redigeeriPaneel.Controls.Add(this.label40);
            this.redigeeriPaneel.Controls.Add(this.label4);
            this.redigeeriPaneel.Controls.Add(this.failieemaldamisNupp);
            this.redigeeriPaneel.Controls.Add(this.label31);
            this.redigeeriPaneel.Controls.Add(this.faililisamisNupp);
            this.redigeeriPaneel.Controls.Add(this.loobuNupp);
            this.redigeeriPaneel.Controls.Add(this.esitatud);
            this.redigeeriPaneel.Controls.Add(this.numericUpDown1);
            this.redigeeriPaneel.Controls.Add(this.dateTimePicker2);
            this.redigeeriPaneel.Controls.Add(this.dateTimePicker1);
            this.redigeeriPaneel.Controls.Add(this.salvestamiseNupp);
            this.redigeeriPaneel.Controls.Add(this.muukulu);
            this.redigeeriPaneel.Controls.Add(this.majutuskulu);
            this.redigeeriPaneel.Controls.Add(this.transpordikulu);
            this.redigeeriPaneel.Controls.Add(this.textBox4);
            this.redigeeriPaneel.Controls.Add(this.label11);
            this.redigeeriPaneel.Controls.Add(this.label10);
            this.redigeeriPaneel.Controls.Add(this.label9);
            this.redigeeriPaneel.Controls.Add(this.label8);
            this.redigeeriPaneel.Controls.Add(this.label7);
            this.redigeeriPaneel.Controls.Add(this.label6);
            this.redigeeriPaneel.Controls.Add(this.label5);
            this.redigeeriPaneel.Location = new System.Drawing.Point(405, 117);
            this.redigeeriPaneel.Name = "redigeeriPaneel";
            this.redigeeriPaneel.Size = new System.Drawing.Size(347, 418);
            this.redigeeriPaneel.TabIndex = 7;
            this.redigeeriPaneel.Visible = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label40.Location = new System.Drawing.Point(120, 263);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(20, 16);
            this.label40.TabIndex = 27;
            this.label40.Text = "...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 264);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Summa kokku:";
            // 
            // failieemaldamisNupp
            // 
            this.failieemaldamisNupp.Location = new System.Drawing.Point(221, 313);
            this.failieemaldamisNupp.Name = "failieemaldamisNupp";
            this.failieemaldamisNupp.Size = new System.Drawing.Size(19, 19);
            this.failieemaldamisNupp.TabIndex = 25;
            this.failieemaldamisNupp.Text = "X";
            this.failieemaldamisNupp.UseVisualStyleBackColor = true;
            this.failieemaldamisNupp.Visible = false;
            this.failieemaldamisNupp.Click += new System.EventHandler(this.failieemaldamisNupp_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(120, 316);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(59, 13);
            this.label31.TabIndex = 24;
            this.label31.Text = "fail puudub";
            // 
            // faililisamisNupp
            // 
            this.faililisamisNupp.Location = new System.Drawing.Point(34, 311);
            this.faililisamisNupp.Name = "faililisamisNupp";
            this.faililisamisNupp.Size = new System.Drawing.Size(75, 23);
            this.faililisamisNupp.TabIndex = 23;
            this.faililisamisNupp.Text = "Lisa fail...";
            this.faililisamisNupp.UseVisualStyleBackColor = true;
            this.faililisamisNupp.Click += new System.EventHandler(this.faililisamisNupp_Click);
            // 
            // loobuNupp
            // 
            this.loobuNupp.Location = new System.Drawing.Point(145, 349);
            this.loobuNupp.Name = "loobuNupp";
            this.loobuNupp.Size = new System.Drawing.Size(75, 23);
            this.loobuNupp.TabIndex = 22;
            this.loobuNupp.Text = "Loobu";
            this.loobuNupp.UseVisualStyleBackColor = true;
            this.loobuNupp.Click += new System.EventHandler(this.loobuNupp_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(120, 221);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(42, 20);
            this.numericUpDown1.TabIndex = 20;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.kuluartikkel_TextChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(120, 189);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 18;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(120, 156);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 17;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // salvestamiseNupp
            // 
            this.salvestamiseNupp.Location = new System.Drawing.Point(34, 349);
            this.salvestamiseNupp.Name = "salvestamiseNupp";
            this.salvestamiseNupp.Size = new System.Drawing.Size(75, 23);
            this.salvestamiseNupp.TabIndex = 16;
            this.salvestamiseNupp.Text = "Salvesta";
            this.salvestamiseNupp.UseVisualStyleBackColor = true;
            this.salvestamiseNupp.Click += new System.EventHandler(this.salvestamiseNupp_Click);
            // 
            // muukulu
            // 
            this.muukulu.Location = new System.Drawing.Point(120, 123);
            this.muukulu.Name = "muukulu";
            this.muukulu.Size = new System.Drawing.Size(62, 20);
            this.muukulu.TabIndex = 13;
            this.muukulu.TextChanged += new System.EventHandler(this.kuluartikkel_TextChanged);
            // 
            // majutuskulu
            // 
            this.majutuskulu.Location = new System.Drawing.Point(120, 91);
            this.majutuskulu.Name = "majutuskulu";
            this.majutuskulu.Size = new System.Drawing.Size(62, 20);
            this.majutuskulu.TabIndex = 12;
            this.majutuskulu.TextChanged += new System.EventHandler(this.kuluartikkel_TextChanged);
            // 
            // transpordikulu
            // 
            this.transpordikulu.Location = new System.Drawing.Point(120, 59);
            this.transpordikulu.Name = "transpordikulu";
            this.transpordikulu.Size = new System.Drawing.Size(62, 20);
            this.transpordikulu.TabIndex = 10;
            this.transpordikulu.TextChanged += new System.EventHandler(this.kuluartikkel_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(120, 27);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(200, 20);
            this.textBox4.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(31, 195);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Lõpu kp:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Alguse kp:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(31, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Muu kulu:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Majutuskulu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Päevade arv:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Transpordikulu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Sihtkoht:";
            // 
            // uueReisiNupp
            // 
            this.uueReisiNupp.Location = new System.Drawing.Point(704, 298);
            this.uueReisiNupp.Name = "uueReisiNupp";
            this.uueReisiNupp.Size = new System.Drawing.Size(75, 23);
            this.uueReisiNupp.TabIndex = 8;
            this.uueReisiNupp.Text = "Uus";
            this.uueReisiNupp.UseVisualStyleBackColor = true;
            this.uueReisiNupp.Click += new System.EventHandler(this.uueReisiNupp_Click);
            // 
            // muutmisNupp
            // 
            this.muutmisNupp.Enabled = false;
            this.muutmisNupp.Location = new System.Drawing.Point(704, 331);
            this.muutmisNupp.Name = "muutmisNupp";
            this.muutmisNupp.Size = new System.Drawing.Size(75, 23);
            this.muutmisNupp.TabIndex = 9;
            this.muutmisNupp.Text = "Muuda";
            this.muutmisNupp.UseVisualStyleBackColor = true;
            this.muutmisNupp.Click += new System.EventHandler(this.muutmisNupp_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(152, 282);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Sinu aruanded:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(152, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Teiste aruanded";
            // 
            // kinnitusPaneel
            // 
            this.kinnitusPaneel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.kinnitusPaneel.Controls.Add(this.label39);
            this.kinnitusPaneel.Controls.Add(this.label35);
            this.kinnitusPaneel.Controls.Add(this.pildivaatamisNupp);
            this.kinnitusPaneel.Controls.Add(this.välja_makstud);
            this.kinnitusPaneel.Controls.Add(this.label28);
            this.kinnitusPaneel.Controls.Add(this.label30);
            this.kinnitusPaneel.Controls.Add(this.label29);
            this.kinnitusPaneel.Controls.Add(this.richTextBox1);
            this.kinnitusPaneel.Controls.Add(this.loobuNupp2);
            this.kinnitusPaneel.Controls.Add(this.esitamata);
            this.kinnitusPaneel.Controls.Add(this.label27);
            this.kinnitusPaneel.Controls.Add(this.label26);
            this.kinnitusPaneel.Controls.Add(this.label25);
            this.kinnitusPaneel.Controls.Add(this.label24);
            this.kinnitusPaneel.Controls.Add(this.label23);
            this.kinnitusPaneel.Controls.Add(this.label22);
            this.kinnitusPaneel.Controls.Add(this.label21);
            this.kinnitusPaneel.Controls.Add(this.label3);
            this.kinnitusPaneel.Controls.Add(this.kinnitatud);
            this.kinnitusPaneel.Controls.Add(this.label14);
            this.kinnitusPaneel.Controls.Add(this.label15);
            this.kinnitusPaneel.Controls.Add(this.label16);
            this.kinnitusPaneel.Controls.Add(this.label17);
            this.kinnitusPaneel.Controls.Add(this.label18);
            this.kinnitusPaneel.Controls.Add(this.label19);
            this.kinnitusPaneel.Controls.Add(this.label20);
            this.kinnitusPaneel.Location = new System.Drawing.Point(771, 24);
            this.kinnitusPaneel.Name = "kinnitusPaneel";
            this.kinnitusPaneel.Size = new System.Drawing.Size(347, 560);
            this.kinnitusPaneel.TabIndex = 21;
            this.kinnitusPaneel.Visible = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label39.Location = new System.Drawing.Point(170, 243);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(20, 16);
            this.label39.TabIndex = 37;
            this.label39.Text = "...";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(45, 243);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 13);
            this.label35.TabIndex = 36;
            this.label35.Text = "Summa kokku:";
            // 
            // pildivaatamisNupp
            // 
            this.pildivaatamisNupp.Location = new System.Drawing.Point(153, 276);
            this.pildivaatamisNupp.Name = "pildivaatamisNupp";
            this.pildivaatamisNupp.Size = new System.Drawing.Size(75, 23);
            this.pildivaatamisNupp.TabIndex = 35;
            this.pildivaatamisNupp.Text = "Vaata";
            this.pildivaatamisNupp.UseVisualStyleBackColor = true;
            this.pildivaatamisNupp.Click += new System.EventHandler(this.pildivaatamisNupp_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(167, 24);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 13);
            this.label28.TabIndex = 33;
            this.label28.Text = "Sihtkoht";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(35, 25);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 13);
            this.label30.TabIndex = 32;
            this.label30.Text = "Nimi:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 327);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(45, 13);
            this.label29.TabIndex = 31;
            this.label29.Text = "Ajalugu:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(14, 343);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(318, 141);
            this.richTextBox1.TabIndex = 30;
            this.richTextBox1.Text = "";
            // 
            // loobuNupp2
            // 
            this.loobuNupp2.Location = new System.Drawing.Point(245, 520);
            this.loobuNupp2.Name = "loobuNupp2";
            this.loobuNupp2.Size = new System.Drawing.Size(75, 23);
            this.loobuNupp2.TabIndex = 29;
            this.loobuNupp2.Text = "Loobu";
            this.loobuNupp2.UseVisualStyleBackColor = true;
            this.loobuNupp2.Click += new System.EventHandler(this.loobuNupp_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(167, 191);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(46, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "Sihtkoht";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(167, 168);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(46, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "Sihtkoht";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(167, 142);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "Sihtkoht";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(167, 113);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "Sihtkoht";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(167, 215);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "Sihtkoht";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(167, 84);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "Sihtkoht";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(167, 52);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 20;
            this.label21.Text = "Sihtkoht";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Fail:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(35, 191);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Lõpu kp:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(34, 168);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Alguse kp:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 142);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Muu kulu:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(35, 113);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Majutuskulu:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(35, 215);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Päevade arv:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(35, 84);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Transpordikulu:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(35, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Sihtkoht:";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(20, 457);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(86, 23);
            this.button5.TabIndex = 22;
            this.button5.Text = "ÄRA VAJUTA!";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // väljalogimisNupp
            // 
            this.väljalogimisNupp.Location = new System.Drawing.Point(25, 516);
            this.väljalogimisNupp.Name = "väljalogimisNupp";
            this.väljalogimisNupp.Size = new System.Drawing.Size(75, 23);
            this.väljalogimisNupp.TabIndex = 23;
            this.väljalogimisNupp.Text = "Logi välja";
            this.väljalogimisNupp.UseVisualStyleBackColor = true;
            this.väljalogimisNupp.Click += new System.EventHandler(this.väljalogimisNupp_Click);
            // 
            // töötajaLisamisNupp
            // 
            this.töötajaLisamisNupp.Location = new System.Drawing.Point(25, 487);
            this.töötajaLisamisNupp.Name = "töötajaLisamisNupp";
            this.töötajaLisamisNupp.Size = new System.Drawing.Size(75, 23);
            this.töötajaLisamisNupp.TabIndex = 25;
            this.töötajaLisamisNupp.Text = "Lisa töötaja";
            this.töötajaLisamisNupp.UseVisualStyleBackColor = true;
            this.töötajaLisamisNupp.Click += new System.EventHandler(this.töötajaLisamisNupp_Click);
            // 
            // töötajaLisamisPaneel
            // 
            this.töötajaLisamisPaneel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.töötajaLisamisPaneel.Controls.Add(this.comboBox1);
            this.töötajaLisamisPaneel.Controls.Add(this.loobuNupp3);
            this.töötajaLisamisPaneel.Controls.Add(this.lisamisNupp);
            this.töötajaLisamisPaneel.Controls.Add(this.textBox6);
            this.töötajaLisamisPaneel.Controls.Add(this.textBox10);
            this.töötajaLisamisPaneel.Controls.Add(this.label36);
            this.töötajaLisamisPaneel.Controls.Add(this.label37);
            this.töötajaLisamisPaneel.Controls.Add(this.label38);
            this.töötajaLisamisPaneel.Location = new System.Drawing.Point(52, 304);
            this.töötajaLisamisPaneel.Name = "töötajaLisamisPaneel";
            this.töötajaLisamisPaneel.Size = new System.Drawing.Size(347, 193);
            this.töötajaLisamisPaneel.TabIndex = 23;
            this.töötajaLisamisPaneel.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(120, 67);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 23;
            // 
            // loobuNupp3
            // 
            this.loobuNupp3.Location = new System.Drawing.Point(245, 137);
            this.loobuNupp3.Name = "loobuNupp3";
            this.loobuNupp3.Size = new System.Drawing.Size(75, 23);
            this.loobuNupp3.TabIndex = 22;
            this.loobuNupp3.Text = "Loobu";
            this.loobuNupp3.UseVisualStyleBackColor = true;
            this.loobuNupp3.Click += new System.EventHandler(this.loobuNupp_Click);
            // 
            // lisamisNupp
            // 
            this.lisamisNupp.Location = new System.Drawing.Point(34, 137);
            this.lisamisNupp.Name = "lisamisNupp";
            this.lisamisNupp.Size = new System.Drawing.Size(75, 23);
            this.lisamisNupp.TabIndex = 16;
            this.lisamisNupp.Text = "Lisa";
            this.lisamisNupp.UseVisualStyleBackColor = true;
            this.lisamisNupp.Click += new System.EventHandler(this.lisamisNupp_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(120, 99);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(62, 20);
            this.textBox6.TabIndex = 12;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(120, 32);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(200, 20);
            this.textBox10.TabIndex = 9;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(31, 102);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(55, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "Positsioon";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(31, 71);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 13);
            this.label37.TabIndex = 2;
            this.label37.Text = "Juht";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(31, 40);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(27, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Nimi";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // otsinguAlgus
            // 
            this.otsinguAlgus.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.otsinguAlgus.Location = new System.Drawing.Point(7, 107);
            this.otsinguAlgus.Name = "otsinguAlgus";
            this.otsinguAlgus.Size = new System.Drawing.Size(127, 20);
            this.otsinguAlgus.TabIndex = 26;
            this.otsinguAlgus.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // otsinguLõpp
            // 
            this.otsinguLõpp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.otsinguLõpp.Location = new System.Drawing.Point(7, 148);
            this.otsinguLõpp.Name = "otsinguLõpp";
            this.otsinguLõpp.Size = new System.Drawing.Size(127, 20);
            this.otsinguLõpp.TabIndex = 27;
            this.otsinguLõpp.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 91);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 13);
            this.label32.TabIndex = 28;
            this.label32.Text = "alates:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 132);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(30, 13);
            this.label33.TabIndex = 29;
            this.label33.Text = "kuni:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 37);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(64, 13);
            this.label34.TabIndex = 30;
            this.label34.Text = "otsi töötajat:";
            // 
            // vaataOmi
            // 
            this.vaataOmi.Enabled = false;
            this.vaataOmi.Location = new System.Drawing.Point(704, 360);
            this.vaataOmi.Name = "vaataOmi";
            this.vaataOmi.Size = new System.Drawing.Size(75, 23);
            this.vaataOmi.TabIndex = 31;
            this.vaataOmi.Text = "Vaata";
            this.vaataOmi.UseVisualStyleBackColor = true;
            this.vaataOmi.Click += new System.EventHandler(this.vaatamisNupp_Click);
            // 
            // esitatud
            // 
            this.esitatud.Location = new System.Drawing.Point(251, 349);
            this.esitatud.Name = "esitatud";
            this.esitatud.Size = new System.Drawing.Size(75, 23);
            this.esitatud.TabIndex = 21;
            this.esitatud.Text = "Esita";
            this.esitatud.UseVisualStyleBackColor = true;
            this.esitatud.Click += new System.EventHandler(this.OlekuNupp_Click);
            // 
            // välja_makstud
            // 
            this.välja_makstud.Location = new System.Drawing.Point(245, 490);
            this.välja_makstud.Name = "välja_makstud";
            this.välja_makstud.Size = new System.Drawing.Size(75, 23);
            this.välja_makstud.TabIndex = 34;
            this.välja_makstud.Text = "Maksa välja";
            this.välja_makstud.UseVisualStyleBackColor = true;
            this.välja_makstud.Click += new System.EventHandler(this.OlekuNupp_Click);
            // 
            // esitamata
            // 
            this.esitamata.Location = new System.Drawing.Point(130, 490);
            this.esitamata.Name = "esitamata";
            this.esitamata.Size = new System.Drawing.Size(83, 23);
            this.esitamata.TabIndex = 28;
            this.esitamata.Text = "Lükka tagasi";
            this.esitamata.UseVisualStyleBackColor = true;
            this.esitamata.Click += new System.EventHandler(this.OlekuNupp_Click);
            // 
            // kinnitatud
            // 
            this.kinnitatud.Location = new System.Drawing.Point(24, 491);
            this.kinnitatud.Name = "kinnitatud";
            this.kinnitatud.Size = new System.Drawing.Size(75, 23);
            this.kinnitatud.TabIndex = 16;
            this.kinnitatud.Text = "Kinnita";
            this.kinnitatud.UseVisualStyleBackColor = true;
            this.kinnitatud.Click += new System.EventHandler(this.OlekuNupp_Click);
            // 
            // vaataTeisteomi
            // 
            this.vaataTeisteomi.Enabled = false;
            this.vaataTeisteomi.Location = new System.Drawing.Point(703, 55);
            this.vaataTeisteomi.Name = "vaataTeisteomi";
            this.vaataTeisteomi.Size = new System.Drawing.Size(75, 23);
            this.vaataTeisteomi.TabIndex = 24;
            this.vaataTeisteomi.Text = "Vaata";
            this.vaataTeisteomi.UseVisualStyleBackColor = true;
            this.vaataTeisteomi.Click += new System.EventHandler(this.vaatamisNupp_Click);
            // 
            // mastervaade1
            // 
            this.AcceptButton = this.sisselogimisNupp;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 600);
            this.Controls.Add(this.vaataOmi);
            this.Controls.Add(this.redigeeriPaneel);
            this.Controls.Add(this.kinnitusPaneel);
            this.Controls.Add(this.sisselogiPaneel);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.otsinguLõpp);
            this.Controls.Add(this.otsinguAlgus);
            this.Controls.Add(this.töötajaLisamisPaneel);
            this.Controls.Add(this.töötajaLisamisNupp);
            this.Controls.Add(this.vaataTeisteomi);
            this.Controls.Add(this.väljalogimisNupp);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.teisteAruanded);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.muutmisNupp);
            this.Controls.Add(this.uueReisiNupp);
            this.Controls.Add(this.omaAruanded);
            this.Controls.Add(this.otsiNupp);
            this.Controls.Add(this.otsinguNimi);
            this.Controls.Add(this.label1);
            this.KeyPreview = true;
            this.Name = "mastervaade1";
            this.Text = "mastervaade1";
            this.Load += new System.EventHandler(this.mastervaade1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.omaAruanded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teisteAruanded)).EndInit();
            this.sisselogiPaneel.ResumeLayout(false);
            this.sisselogiPaneel.PerformLayout();
            this.redigeeriPaneel.ResumeLayout(false);
            this.redigeeriPaneel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.kinnitusPaneel.ResumeLayout(false);
            this.kinnitusPaneel.PerformLayout();
            this.töötajaLisamisPaneel.ResumeLayout(false);
            this.töötajaLisamisPaneel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox otsinguNimi;
        private System.Windows.Forms.Button otsiNupp;
        internal System.Windows.Forms.DataGridView omaAruanded;
        private System.Windows.Forms.Panel sisselogiPaneel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button sisselogimisNupp;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel redigeeriPaneel;
        private System.Windows.Forms.TextBox muukulu;
        private System.Windows.Forms.TextBox majutuskulu;
        private System.Windows.Forms.TextBox transpordikulu;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button uueReisiNupp;
        private System.Windows.Forms.Button muutmisNupp;
        private System.Windows.Forms.Button salvestamiseNupp;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        internal System.Windows.Forms.DataGridView teisteAruanded;
        private System.Windows.Forms.Panel kinnitusPaneel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button loobuNupp;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button loobuNupp2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button väljalogimisNupp;
        private System.Windows.Forms.Panel töötajaLisamisPaneel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button loobuNupp3;
        private System.Windows.Forms.Button lisamisNupp;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button töötajaLisamisNupp;
        private System.Windows.Forms.Button faililisamisNupp;
        private System.Windows.Forms.Button pildivaatamisNupp;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button failieemaldamisNupp;
        private System.Windows.Forms.DateTimePicker otsinguAlgus;
        private System.Windows.Forms.DateTimePicker otsinguLõpp;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label4;
        private Erinupp vaataTeisteomi;
        private Erinupp vaataOmi;
        private Erinupp kinnitatud;
        private Erinupp esitatud;
        private Erinupp esitamata;
        private Erinupp välja_makstud;
    }
}