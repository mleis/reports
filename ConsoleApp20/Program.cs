﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp20
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new mastervaade1());
        }
    }


    class Aruanne
    {
        public string Sihtkoht
        {
            get;
            set;
        }
        public double Transport
        {
            get;
            set;
        }
        public List<Majutuskulu> Majutus
        {
            get;
            set;
        }

        public double Söök
        {
            get;
            set;
        }
        public double MuudKulud
        {
            get;
            set;
        }
        public DateTime AruandeKuupaev
        {
            get;
            set;
        }
        public string Reisija
        {
            get;
            set;
        }
        public List<string> Ajalugu
        {
            get;
            set;
        }
        public List<string> Lisad
        {
            get;
            set;
        }
        public bool OnEsitatud
        {
            get;
            set;
        }
        public bool Kinnitatud
        {
            get;
            set;
        }
        public bool ValjaMakstud
        {
            get;
            set;
        }

    }
    class Majutuskulu //juhul kui ööbib reisi jooksul ühes kohas ehk on nimekiri
    {
        public int OobimisteArv
        {
            get;
            set;
        }
        public double HindOobimiseKohta
        {
            get;
            set;
        }

        public string MajutuseNimi
        {
            get;
            set;
        }
    }
}
