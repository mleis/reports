﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ConsoleApp20
{
    public partial class mastervaade1 : Form
    {
        MKReisikuludEntities me = new MKReisikuludEntities();
        T88tajad kasutaja { get; set; }
        Aruanded aktiivneAruanne { get; set; }
        string failiNimi = "";
        string otsiNimi;
        DateTime otsiAlgus;
        DateTime otsiLõpp;
        bool otsitud;
        string[] järjestus = { "Kuupäev", "alla" };

        public mastervaade1()
        {
            InitializeComponent();
        }

        private void mastervaade1_Load(object sender, EventArgs e)
        {
            avaEsmavaade();
        }

        private void sisselogimisNupp_Click(object sender, EventArgs e)
        {
            if (!me.T88tajad.Any(x => x.Nimi == textBox2.Text))
            {
                MessageBox.Show("Sellist kasutajat ei leidu!");
                textBox2.Clear();
            }
            else
            {
                kasutaja = new T88tajad();
                kasutaja.Nimi = textBox2.Text;
                kasutaja.Id = me.T88tajad
                    .Where(x => x.Nimi == kasutaja.Nimi)
                    .Select(x => new { x.Id }).First().Id;
                kasutaja.Positsioon = me.T88tajad
                    .Where(x => x.Nimi == kasutaja.Nimi)
                    .Select(x => new { x.Positsioon }).First().Positsioon;

                label1.Text = "Tere, " + kasutaja.Nimi + "!";
                label13.Text = "Aruanded sinule kinnitamiseks" + (kasutaja.Positsioon == "Raamatupidaja" ? " või väljamaksmiseks" : "") + ":";
                otsiNimi = "";
                otsiAlgus = DateTime.Today.AddMonths(-6);
                otsiLõpp = DateTime.Today;
                otsitud = false;
                otsinguNimi.Clear();
                lähtestaKuupäev(otsinguAlgus);
                lähtestaKuupäev(otsinguLõpp);

                järjestus[0] = "Kuupäev";
                järjestus[1] = "alla";

                värskendaTabelit(omaAruanded);
                värskendaTabelit(teisteAruanded);

                sisselogiPaneel.Hide();
                muutmisNupp.Enabled = false;
                vaataTeisteomi.Enabled = false;
                ActiveForm.Size = new Size(1000, 600);
                CenterToScreen();
            }            
        }
        private void uueReisiNupp_Click(object sender, EventArgs e)
        {
            foreach (var control in redigeeriPaneel.Controls)
            {
                if (control is TextBox)
                    (control as TextBox).Clear();
                if (control is DateTimePicker)
                {
                    (control as DateTimePicker).Format = DateTimePickerFormat.Custom;
                    (control as DateTimePicker).CustomFormat = " ";
                }
                if (control is NumericUpDown)
                    (control as NumericUpDown).Value = 0;
            }
            failiNimi = "";
            label31.Text = "fail puudub";
            uuendaSummat();

            aktiivneAruanne = new Aruanded();
            aktiivneAruanne.id = me.Aruanded
                .Select(x => x.id)
                .Max() + 1;
            redigeeriPaneel.Location = new Point(270, 35);
            redigeeriPaneel.Show();
        }
        private void omaAruanded_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (muutmisNupp.Enabled)
                muudaAruannet();
            else
                vaataAruannet(omaAruanded);
        }

        private void muutmisNupp_Click(object sender, EventArgs e)
        {
            muudaAruannet();
        }

        private void salvestamiseNupp_Click(object sender, EventArgs e)
        {
            aktiivneAruanne.Töötaja_id = kasutaja.Id;
            aktiivneAruanne.Sihtkoht = textBox4.Text;
            aktiivneAruanne.Oobimistearv = (int)numericUpDown1.Value;
            int d;
            int.TryParse(transpordikulu.Text, out d);
            aktiivneAruanne.Transport = d;
            int.TryParse(majutuskulu.Text, out d);
            aktiivneAruanne.MajutuseHind = d;
            int.TryParse(muukulu.Text, out d);
            aktiivneAruanne.MuudKulud = d;
            if (dateTimePicker1.Format != DateTimePickerFormat.Custom && dateTimePicker2.Format != DateTimePickerFormat.Custom)
            {
                aktiivneAruanne.AlguseKuupäev = dateTimePicker1.Value;
                aktiivneAruanne.Lõpukuupäev = dateTimePicker2.Value;
            }            
            aktiivneAruanne.Olek = "esitamata";
            if (failiNimi != "")
            {
                var buff = File.ReadAllBytes(failiNimi);
                aktiivneAruanne.Lisa = buff;
            }
            else
            {
                aktiivneAruanne.Lisa = null;
            }

            if (!me.Aruanded.Any(x => x.id == aktiivneAruanne.id))
            {
                kirjutaAjalugu("loomine");
                me.Aruanded.Add(aktiivneAruanne);
            }
            else
            {
                kirjutaAjalugu("muutmine");
            }
            me.SaveChanges();
            värskendaTabelit(omaAruanded);
            redigeeriPaneel.Hide();
        }
        private void teisteAruanded_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            vaataAruannet(teisteAruanded);
        }
        private void vaatamisNupp_Click(object sender, EventArgs e)
        {
            vaataAruannet((sender as Erinupp).Tabel);
        }
        private void avaEsmavaade()
        {
            sisselogiPaneel.Show();
            Size = new Size(360, 240);
            CenterToScreen();
            textBox2.Clear();
            textBox2.Select();
        }
        private void muudaAruannet()
        {
            int id = (int)omaAruanded.CurrentRow.Cells[0].Value;
            aktiivneAruanne = me.Aruanded
                .Where(x => x.id == id)
                .First();

            textBox4.Text = aktiivneAruanne.Sihtkoht;
            transpordikulu.Text = aktiivneAruanne.Transport.ToString();
            majutuskulu.Text = aktiivneAruanne.MajutuseHind.ToString();
            muukulu.Text = aktiivneAruanne.MuudKulud.ToString();
            kirjutaKuupäev(dateTimePicker1, aktiivneAruanne.AlguseKuupäev);
            kirjutaKuupäev(dateTimePicker2, aktiivneAruanne.Lõpukuupäev);
            numericUpDown1.Value = aktiivneAruanne.Oobimistearv.GetValueOrDefault();
            label40.Text = ((aktiivneAruanne.Oobimistearv * 15 + aktiivneAruanne.Transport + aktiivneAruanne.MajutuseHind + aktiivneAruanne.MuudKulud) ?? 0).ToString() + " €";
            label31.Text = aktiivneAruanne.Lisa == null ? "fail puudub" : "fail lisatud";
            failiNimi = "";
            failieemaldamisNupp.Location = new Point(label31.Location.X + label31.Width + 5, failieemaldamisNupp.Location.Y);
            failieemaldamisNupp.Visible = aktiivneAruanne.Lisa != null;

            redigeeriPaneel.Location = new Point(270, 35);
            redigeeriPaneel.Show();
        }
        private void kirjutaKuupäev(DateTimePicker field, DateTime? kp)
        {
            if (kp != null)
            {
                field.Format = DateTimePickerFormat.Short;
                field.Value = (DateTime)kp;
            }
            else
            {
                field.Format = DateTimePickerFormat.Custom;
                field.CustomFormat = " ";
            }
        }
        private void lähtestaKuupäev(DateTimePicker field)
        {
            field.Value = DateTime.Today;
            field.Format = DateTimePickerFormat.Custom;
            field.CustomFormat = " ";
        }
        private void vaataAruannet(DataGridView tabel)
        {
            int id = (int)tabel.CurrentRow.Cells[0].Value;
            aktiivneAruanne = me.Aruanded
                .Where(x => x.id == id)
                .First();

            label28.Text = me.T88tajad.Where(x => x.Id == aktiivneAruanne.Töötaja_id).Select(x => x.Nimi).First();
            label21.Text = aktiivneAruanne.Sihtkoht;
            label22.Text = aktiivneAruanne.Transport.ToString();
            label23.Text = aktiivneAruanne.Oobimistearv.ToString();
            label24.Text = aktiivneAruanne.MajutuseHind.ToString();
            label25.Text = aktiivneAruanne.MuudKulud.ToString();            
            label26.Text = aktiivneAruanne.AlguseKuupäev.ToString();
            label27.Text = aktiivneAruanne.Lõpukuupäev.ToString();
            label39.Text = ((aktiivneAruanne.Oobimistearv * 15 + aktiivneAruanne.Transport + aktiivneAruanne.MajutuseHind + aktiivneAruanne.MuudKulud) ?? 0).ToString() + " €";
            richTextBox1.Text = aktiivneAruanne.Ajalugu;

            int töötajaId = aktiivneAruanne.Töötaja_id;
            pildivaatamisNupp.Enabled = aktiivneAruanne.Lisa != null;
            kinnitatud.Enabled = me.T88tajad.Where(x => x.Id == töötajaId).Select(x => x.JuhiID).First() == kasutaja.Id && aktiivneAruanne.Olek == "esitatud";
            esitamata.Enabled = kinnitatud.Enabled;
            välja_makstud.Enabled = kasutaja.Positsioon == "Raamatupidaja" && aktiivneAruanne.Olek == "kinnitatud";

            kinnitusPaneel.Location = new Point(270, 15);
            kinnitusPaneel.Show();
        }

        private void värskendaTabelit(DataGridView tabel)
        {
            var joined = me.Aruanded
                    .Join(me.T88tajad,
                        a => a.Töötaja_id,
                        t => t.Id,
                        (a, t) => new { Aruanne = a, Töötaja = t });

            if (tabel == omaAruanded)
            {
                joined = joined.Where(x => x.Aruanne.Töötaja_id == kasutaja.Id);
            }
            else
            {
                joined = joined
                    .Where(x => (kasutaja.Positsioon == "Raamatupidaja" || x.Töötaja.JuhiID == kasutaja.Id)
                    &&
                    (otsiNimi == "" || x.Töötaja.Nimi.ToLower().Contains(otsiNimi.ToLower()))
                    &&
                    (x.Aruanne.AlguseKuupäev >= otsiAlgus && x.Aruanne.AlguseKuupäev <= otsiLõpp)
                    );
            }

            if (!otsitud)
                joined = joined.Take(20);

            var j = joined.Select(x => new { Id = x.Aruanne.id, x.Töötaja.Nimi, x.Aruanne.Sihtkoht, Kuupäev = x.Aruanne.AlguseKuupäev, Olek = x.Aruanne.Olek });

            if (järjestus[0] == "Nimi" && järjestus[1] == "alla")
                tabel.DataSource = j.OrderByDescending(x => x.Nimi).ToList();
            if (järjestus[0] == "Nimi" && järjestus[1] == "üles")
                tabel.DataSource = j.OrderBy(x => x.Nimi).ToList();
            if (järjestus[0] == "Sihtkoht" && järjestus[1] == "alla")
                tabel.DataSource = j.OrderByDescending(x => x.Sihtkoht).ToList();
            if (järjestus[0] == "Sihtkoht" && järjestus[1] == "üles")
                tabel.DataSource = j.OrderBy(x => x.Sihtkoht).ToList();
            if (järjestus[0] == "Kuupäev" && järjestus[1] == "alla")
                tabel.DataSource = j.OrderByDescending(x => x.Kuupäev).ToList();
            if (järjestus[0] == "Kuupäev" && järjestus[1] == "üles")
                tabel.DataSource = j.OrderBy(x => x.Kuupäev).ToList();
            if (järjestus[0] == "Olek" && järjestus[1] == "alla")
                tabel.DataSource = j.OrderByDescending(x => x.Olek).ToList();
            if (järjestus[0] == "Olek" && järjestus[1] == "üles")
                tabel.DataSource = j.OrderBy(x => x.Olek).ToList();

            tabel.Columns[0].Visible = false;

            if (tabel.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in tabel.Rows)
                {
                    switch (row.Cells[4].Value.ToString())
                    {
                        case "esitamata":
                            row.Cells[4].Style.BackColor = Color.Salmon;
                            break;
                        case "esitatud":
                            row.Cells[4].Style.BackColor = Color.LightYellow;
                            break;
                        case "kinnitatud":
                            row.Cells[4].Style.BackColor = Color.LightSkyBlue;
                            break;
                        case "välja makstud":
                            row.Cells[4].Style.BackColor = Color.LightSeaGreen;
                            break;
                        default:
                            row.Cells[4].Style.BackColor = Color.Gray;
                            break;
                    }
                }
            }
        }
        private void OlekuNupp_Click(object sender, EventArgs e)
        {
            aktiivneAruanne.Olek = (sender as Erinupp).Name == "välja_makstud" ? "välja makstud" : (sender as Erinupp).Name;
            kirjutaAjalugu((sender as Erinupp).Name);
            me.SaveChanges();
            värskendaTabelit((sender as Erinupp).Tabel);
            (sender as Erinupp).Parent.Hide();
        }
        private void loobuNupp_Click(object sender, EventArgs e)
        {
            (sender as Button).Parent.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Palun ära rohkem enam vajuta.");
        }

        private void väljalogimisNupp_Click(object sender, EventArgs e)
        {
            avaEsmavaade();
        }

        private void omaAruanded_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                järjestus[0] = omaAruanded.Columns[e.ColumnIndex].HeaderCell.Value.ToString();
                järjestus[1] = järjestus[1] == "üles" ? "alla" : "üles";
                värskendaTabelit(omaAruanded);
            }
            else
            {
                muutmisNupp.Enabled = omaAruanded.Rows[e.RowIndex].Cells[4].Value.ToString() == "esitamata";
                vaataOmi.Enabled = !muutmisNupp.Enabled;
            }

        }
        private void teisteAruanded_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                järjestus[0] = teisteAruanded.Columns[e.ColumnIndex].HeaderCell.Value.ToString();
                järjestus[1] = järjestus[1] == "üles" ? "alla" : "üles";
                värskendaTabelit(teisteAruanded);
            }
            else
            {
                vaataTeisteomi.Enabled = true;
            }
        }
        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            (sender as DateTimePicker).Format = DateTimePickerFormat.Short;
            uuendaPäevi();
            uuendaSummat();
        }
        private void lisamisNupp_Click(object sender, EventArgs e)
        {
            T88tajad uusTöötaja = new T88tajad();

            uusTöötaja.Id = me.T88tajad
                .Select(x => x.Id)
                .Max() + 1;
            uusTöötaja.Nimi = textBox10.Text;
            uusTöötaja.JuhiID = me.T88tajad
                .Where(x => x.Nimi == comboBox1.SelectedItem.ToString())
                .Select(x => x.Id)
                .First();

            me.T88tajad.Add(uusTöötaja);
            me.SaveChanges();
            töötajaLisamisPaneel.Hide();
        }

        private void töötajaLisamisNupp_Click(object sender, EventArgs e)
        {
            foreach (var control in töötajaLisamisPaneel.Controls)
            {
                if (control is TextBox)
                    (control as TextBox).Clear();
                if (control is ComboBox)
                    (control as ComboBox).SelectedItem = null;
            }

            comboBox1.Items.Clear();
            List<string> töötajad = new List<string>(me.T88tajad.Select(x => x.Nimi));
            comboBox1.Items.AddRange(töötajad.ToArray());

            töötajaLisamisPaneel.Location = new Point(250, 100);
            töötajaLisamisPaneel.Show();
        }

        private void faililisamisNupp_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Vali fail";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                failiNimi = openFileDialog1.FileName;
                label31.Text = leiaFailiNimi(failiNimi);
                failieemaldamisNupp.Location = new Point(label31.Location.X + label31.Width + 5, failieemaldamisNupp.Location.Y);
                failieemaldamisNupp.Show();
            }
        }
        private void pildivaatamisNupp_Click(object sender, EventArgs e)
        {
            Bitmap bm = new Bitmap(new MemoryStream(aktiivneAruanne.Lisa));
            pilt pilt = new pilt(bm);
            pilt.ShowDialog();
        }

        private void failieemaldamisNupp_Click(object sender, EventArgs e)
        {
            failiNimi = "";
            label31.Text = "fail puudub";
            failieemaldamisNupp.Hide();
        }
        static string leiaFailiNimi(string path)
        {
            string str = "";
            for (int i = path.Length - 1; i >= 0; i--)
            {
                if (path[i] == '\\')
                    break;
                str = path[i] + str;
            }
            return str;
        }
        private void uuendaPäevi()
        {
            if (dateTimePicker1.Value != null && dateTimePicker2.Value != null && (dateTimePicker2.Value - dateTimePicker1.Value).Days >= 0)
            {
                numericUpDown1.Value = (dateTimePicker2.Value - dateTimePicker1.Value).Days;
            }
        }
        private void uuendaSummat()
        {
            int transport, majutus, muu;
            int.TryParse(transpordikulu.Text, out transport);
            int.TryParse(majutuskulu.Text, out majutus);
            int.TryParse(muukulu.Text, out muu);
            label40.Text = (numericUpDown1.Value * 15 + transport + majutus + muu).ToString() + " €";
        }
        private void kuluartikkel_TextChanged(object sender, EventArgs e)
        {
            uuendaSummat();
        }
        private void otsiNupp_Click(object sender, EventArgs e)
        {
            otsiNimi = otsinguNimi.Text;
            otsiAlgus = otsinguAlgus.Value;
            otsiLõpp = otsinguLõpp.Value;
            otsitud = true;
            värskendaTabelit(teisteAruanded);
        }
        private void kirjutaAjalugu(string tegevus)
        {
            aktiivneAruanne.Ajalugu += aktiivneAruanne.Ajalugu == null ? "" : "\n";
            aktiivneAruanne.Ajalugu += DateTime.Now + " :    " + kasutaja.Nimi + " ";

            switch (tegevus)
            {
                case "loomine":
                    aktiivneAruanne.Ajalugu += "lõi aruande."; 
                    break;
                case "muutmine":
                    aktiivneAruanne.Ajalugu += "muutis aruannet."; 
                    break;
                case "esitatud":
                    aktiivneAruanne.Ajalugu += "esitas aruande."; 
                    break;
                case "kinnitatud":
                    aktiivneAruanne.Ajalugu += "kinnitas aruande."; 
                    break;
                case "esitamata":
                    aktiivneAruanne.Ajalugu += "saatis aruande tagasi."; 
                    break;
                case "välja makstud":
                case "välja_makstud":
                    aktiivneAruanne.Ajalugu += "kinnitas väljamakse."; 
                    break;
            }
        }
    }
}