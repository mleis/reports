﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace ConsoleApp20
{
    public partial class pilt : Form
    {
        public Bitmap Image { get; set; }
        public pilt(Bitmap image)
        {
            InitializeComponent();
            Image = image;
        }
        public pilt()
        {
            InitializeComponent();
        }

        private void pilt_Load(object sender, EventArgs e)
        {
            CenterToScreen();
            pictureBox1.Image = Image;
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;          
            Size = new Size( pictureBox1.Width + 15, pictureBox1.Height + 40);
        }
    }
}
